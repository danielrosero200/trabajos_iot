#define  RED      2
#define  YELLOW   4
#define  BLUE     16

typedef int TaskProfiler;

TaskProfiler RedTaskProfiler;
TaskProfiler BlueTaskProfiler;
TaskProfiler  YellowTaskProfiler;

QueueHandle_t queue;

TaskHandle_t Task1;
TaskHandle_t Task2;
TaskHandle_t Task3;
SemaphoreHandle_t xBinarySemaphore;

void setup()
{
  Serial.begin(115200);

  xBinarySemaphore = xSemaphoreCreateBinary();
  pinMode(RED,OUTPUT);
  pinMode(BLUE,OUTPUT);
  pinMode(YELLOW,OUTPUT);
  
  xTaskCreatePinnedToCore(redLedControllerTask, "Red Led Task",1000,NULL,1,&Task2,0);
  xTaskCreatePinnedToCore(blueLedControllerTask, "Blue Led Task", 1000,NULL,1,&Task3,0);
  xTaskCreatePinnedToCore(yellowLedControllerTask,"Yellow Led Task", 1000,NULL,1,&Task1,0);
}

void redLedControllerTask(void *pvParameters)
{
  
  xSemaphoreGive(xBinarySemaphore);
  while(1)
  {
   xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
   digitalWrite(RED,digitalRead(RED)^1);
   delay(1000);
   //blink(RED,1000);
   
   Serial.println("This is RED Task");
   xSemaphoreGive(xBinarySemaphore);
   vTaskDelay(1);

  }
}
void blueLedControllerTask(void *pvParameters)
{
  
  while(1)
  {
    xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
    digitalWrite(BLUE,digitalRead(BLUE)^1);
    delay(1000);
    //blink(BLUE,1000);
    Serial.println("This is BLUE Task");
    xSemaphoreGive(xBinarySemaphore);
    vTaskDelay(1);
  }
}
void yellowLedControllerTask(void *pvParameters)
{
  
  while(1)
  {
     xSemaphoreTake(xBinarySemaphore, portMAX_DELAY);
     digitalWrite(YELLOW,digitalRead(YELLOW)^1);
     delay(1000);
     //blink(YELLOW,1000);
     Serial.println("This is YELLOW Task");
     xSemaphoreGive(xBinarySemaphore);
     vTaskDelay(1);
  }
}

void blink(byte pin, int duration)
{
digitalWrite(pin,HIGH);
delay(duration);
digitalWrite(pin, LOW);
delay(duration);  
}

void loop(){}
